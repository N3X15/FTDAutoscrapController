@echo off
rm -rf bin dist
mkdir dist
mkdir dist\AutoscrapControl
mkdir dist\AutoscrapControl\Items
mkdir "dist\AutoscrapControl\Character Items"
copy /y FTDAutoscrapControl\bin\FTDAutoscrapControl.dll dist\AutoscrapControl\
copy /y meta\plugin.json dist\AutoscrapControl\
copy /y meta\header.header dist\AutoscrapControl\
copy /y meta\Items\*.item dist\AutoscrapControl\Items
copy /y "meta\Character Items\*.item" "dist\AutoscrapControl\Character Items"
pause