# FTD Autoscrap Controller

A simple plugin demonstrating the new FTD DLL framework.

## How it Works

1. FTD goes through the `Documents/From The Depths/Mods/` folder and checks for a directory that contains a plugin.json
2. FTD loads the plugin from the filename specified.
3. After all plugins are loaded, FTD proceeds to load in object definitions and assets.
4. Finally, FTD combines and links all of the modded assets.

## Creating a Project

1. Create a new .NET 3.5 C# library project in your favorite IDE.
2. Link FTD's `Assembly-CSharp.dll` and `UnityEngine.dll` from `From the Depths\From_The_Depths_Data\Managed\`.
3. Create `NAMEPlugin.cs` and implement the `FTDPlugin` interface.  See [AutoscrapControllerPlugin.cs](FTDAutoscrapControl/AutoscrapControllerPlugin.cs) for an example.
4. Implement the classes you wish to implement, extending either `CharacterItem` or `Block` (or any subclasses).
5. Create `plugin.json` using the [example provided](meta/plugin.json)
6. Open FTD and create a new Mod.
7. Make the necessary entries. (CharacterItems don't require a Prefab ID, leaving it blank will create an empty GameObject for you and inject the class you desire.)
8. Save mod to disk and drop in the files you compiled.

After you're done, the structure should look like this:

```
Documents/From the Depths/
  MyMod/
    header.header - Describes your project
    MyMod.dll     - Plugin containing custom classes
    plugin.json   - Dependency information for your DLL
    src/
      (Code for your plugin)
    Character Items/
      0000_MyCharItem_[random stuff].charitem - Actual charitem
      guidmap.json                            - GUID preservation stuff
    Prefabs/
      (Prefab SDK soon[tm])
    Assets/
      .obj files             - Meshes
      .animclip files (soon) - Animation Clips
      .png files             - Textures
      etc
```

## Installing

1. Close FTD
2. Extract the archive you obtained to `Documents/From the Depths/Mods`.
3. Start FTD

## Licensing

Your project should include source code when distributed, so open-source licensing should be used.

# THIS Project

## Licensing
Autoscrap Controller was written by N3X15 as an example (and proof-of-concept) and is therefore MIT-licensed.  PRs are welcome.

See [LICENSE.md](LICENSE.md) for more information.

## Support
File a bug report using the provided form.
