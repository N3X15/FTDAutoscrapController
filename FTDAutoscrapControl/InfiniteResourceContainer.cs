// Not tested - N3X
public class InfiniteResourceContainer : ResourceContainer
{
    public void Update()
    {
        Crystal.fQuantity = Crystal.Maximum;
        Metal.fQuantity = Metal.Maximum;
        Natural.fQuantity = Natural.Maximum;
        Oil.fQuantity = Oil.Maximum;
        Scrap.fQuantity = Scrap.Maximum;
    }
}
