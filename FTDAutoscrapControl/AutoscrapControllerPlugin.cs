﻿using System;
using UnityEngine;

// This is required, and provides necessary information to FTD, and provides some important hooks.
public class AutoscrapControllerPlugin:FTDPlugin
{
    public void OnLoad()
    {
        Debug.Log("Loading FTDAutoScrapControl...");
    }

    public void OnSave()
    {
    }

    // Required.
    public string name
    {
        get { return "AutoScrap Controller"; }
    }

    // Also required.
    public Version version
    {
        get { return new Version("0.0.1"); }
    }
}
