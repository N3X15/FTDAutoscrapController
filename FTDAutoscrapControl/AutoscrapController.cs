﻿using System; // Nullables
using BrilliantSkies.FromTheDepths.Planets; // This gets us InstanceSpecification.

// A sample use of CharacterItems, which is what character weapons and skills are.
//  Note that this is NOT a MonoBehaviour.  Due to a Unity bug, we cannot load external MonoBehaviours.
public class AutoscrapController : CharacterItem
{
    // Called on Left-Clicking (primary fire).
    public override void LeftClick()
    {
        ConstructableCleanUp? newval = null;
        switch (InstanceSpecification.i.Header.CommonSettings.ConstructableCleanUp)
        {
            case ConstructableCleanUp.Ai:
                newval = ConstructableCleanUp.Off;
                break;
            case ConstructableCleanUp.Off:
                newval = ConstructableCleanUp.Ai;
                break;
        }
        if (newval != null)
        {
            InstanceSpecification.i.Header.CommonSettings.ConstructableCleanUp = (ConstructableCleanUp)newval;
            HUD.InfoStore.Add(new InfoSnippet(string.Format("Autoscrapping set to {0}.", Enum.GetName(typeof(ConstructableCleanUp), newval)), 3f));
        }
    }
}
